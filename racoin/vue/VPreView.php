<?php 
namespace racoin\vue;

class VPreView extends \racoin\vue\View {

	public function __construct(Array $d) {

		parent::__construct($d);
		$this->layout = 'preview.html.twig';
		$this->arrayVar['d'] = $d;

	}
}