<?php

namespace racoin\vue;

class VListannonce extends \racoin\vue\View {

	public function __construct(Array $a) {
		parent::__construct($a);
		$this->layout = 'listannonce.html.twig';
		$this->arrayVar['a'] = $a;
	}

}