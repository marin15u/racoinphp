<?php 
namespace racoin\vue;
class VFormMesAnnonces extends \racoin\vue\View {

	public function __construct(Array $d) {
		parent::__construct($d);
		$this->layout = 'formMesAnnonces.html.twig';
		$this->arrayVar['d'] = $d;
		//$this->arrayVar['url'] = Slim::getInstance()->urlFor('commentaire',array($m->id));
	}
}