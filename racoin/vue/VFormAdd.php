<?php 
namespace racoin\vue;

class VFormAdd extends \racoin\vue\View {

	public function __construct(Array $d) {
		
		parent::__construct($d);
		$this->layout = 'formadd.html.twig';
		$this->arrayVar['d'] = $d;
		//$this->arrayVar['url'] = Slim::getInstance()->urlFor('commentaire',array($m->id));
	}
}