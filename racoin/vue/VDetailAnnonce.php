<?php
namespace racoin\vue;

class VDetailAnnonce extends View {

	public function __construct(Array $d) {
		parent::__construct($d);
		$this->layout = 'detailannonce.html.twig';
		$this->arrayVar['d'] = $d;
	}
} 
