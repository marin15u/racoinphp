<?php 
namespace racoin\controleur;

Class AnnonceControleur{

	public function actionListeAnnonce(){
		
		$a = \racoin\model\Annonce::All();
		$v = new \racoin\vue\VListAnnonce($a->toArray());
		$v->display();
	}


	public function actionDetailAnnonce($id){

		$d = \racoin\model\Annonce::findById($id);
		$v = new \racoin\vue\VDetailAnnonce($d->toArray());
		$v->display();
	}


	public function actionListeAnnonceAPI(){
		$app= \Slim\Slim::getInstance();
		$headers = $app->response->headers->set('Content-Type', 'application/json');
		$status = $app->response->setStatus(200);
		$array = array();
		$a = \racoin\model\Annonce::All();

		for($i=0; $i<count($a); $i++){
			$array['Annonce'.$i] = array();
			$array['Annonce'.$i]['id']= $a[$i]->id;
			$array['Annonce'.$i]['titre'] = $a[$i]->titre;
			$array['Annonce'.$i]['description'] = $a[$i]->description;
			$array['Annonce'.$i]['ville'] = $a[$i]->ville;
			$array['Annonce'.$i]['cp'] = $a[$i]->CP;
			$array['Annonce'.$i]['photo'] = $a[$i]->photo;
			$array['Annonce'.$i]['datePubli'] = $a[$i]->datePubli;
			$array['Annonce'.$i]['nomAnnonceur'] = $a[$i]->nomAnnonceur;
			$array['Annonce'.$i]['idCategorie'] = $a[$i]->idCategorie;
		}	

		/*echo "Voici toutes les annonces au format JSON.";
		echo "<hr>";*/
		echo json_encode($array);
	}

	public function actionDetailAnnonceAPI($id){

		$app= \Slim\Slim::getInstance();
		$headers = $app->response->headers->set('Content-Type', 'application/json');
		$status = $app->response->setStatus(200);

		$a = \racoin\model\Annonce::findById($id);
		if(empty($a[0])){
			echo 'Cet id d\'annonce n\'existe pas !';
			   

		}

		else{

		$array['Annonce']['id']= $a[0]->id;
		$array['Annonce']['titre'] = $a[0]->titre;
		$array['Annonce']['description'] = $a[0]->description;
		$array['Annonce']['ville'] = $a[0]->ville;
		$array['Annonce']['cp'] = $a[0]->CP;
		$array['Annonce']['photo'] = $a[0]->photo;
		$array['Annonce']['datePubli'] = $a[0]->datePubli;
		$array['Annonce']['nomAnnonceur'] = $a[0]->nomAnnonceur;
		$array['Annonce']['idCategorie'] = $a[0]->idCategorie;
		
		echo json_encode($array);
		}
	}

	public function actionListAnnoncesParCateg($categorie) {

		$d = \racoin\model\Annonce::findByCateg($categorie) ;
		$v = new \racoin\vue\VListAnnonceParCateg($d->toArray()) ;
		$v->display();
	}

	public function actionAdd(){

		$categs = \racoin\model\Categorie::All();

		$app= \Slim\Slim::getInstance();
		$url = $app->urlFor('post', array());
		$array = [
			'url' => $url,
			'categs' => $categs->toArray(),
		];
		$v = new \racoin\vue\VFormAdd($array);
		$v->display();
	}

	public function actionListMesAnnoncesForm() {

		$app= \Slim\Slim::getInstance();
		$url = $app->urlFor('mesannoncespost', array());
		$array = [
			'url' => $url,
		];

		$v = new \racoin\vue\VFormMesAnnonces($array) ;
		$v->display();
	}

	public function actionPreviewAdd($titre,$ville,$cp,$prix,$nom,$description,$categ,$mdp,$photo,$mail){

		$app= \Slim\Slim::getInstance();
		$url = $app->urlFor('valid', array());

		$array = [
			'url' => $url,
			'titre' => $titre,
			'ville' => $ville,
			'cp'    => $cp,
			'prix'  => $prix,
			'nom'   => $nom,
			'description' => $description,
			'categ' => $categ,
			'mdp' => $mdp,
			'date' => date("y.m.d"),
			'photo' => $photo,
			'mail' => $mail,
		];
		$_SESSION['array'] = $array;
		$v = new \racoin\vue\VPreView($array);
		$v->display();

	}
	public function actionValidAdd(){
		$app= \Slim\Slim::getInstance();
		if(isset($_POST['valider'])){
			$d = \racoin\model\Annonce::add($_SESSION['array']);
			//$app->redirect('racoinphp/add');
		}
	}

	public function actionListMesAnnonces($mail) {

		$d = \racoin\model\Annonce::findByMail($mail);
		$v = new \racoin\vue\VMesAnnonces($d->toArray());
		$v->display();
	}

	public function actionAideApi(){
		
		$a = \racoin\model\Annonce::All();
		$v = new \racoin\vue\VAideApi($a->toArray());
		$v->display();
	}


}