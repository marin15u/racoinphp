<?php


namespace racoin\model;

class Categorie extends \Illuminate\Database\Eloquent\Model{
	//Definit le nom de la table et de la clé primaire 
	protected $table = 'racoincategorie';
	protected $primaryKey = 'id';
	public $timestamps=false;

	function __construct(){}

	public function annonces()
	{
		return $this->HasMany('Annonce','idCatgorie');
	}

	public static function findById($id) {

		return Categorie::where('id','=' ,$id)->get();
	}
}