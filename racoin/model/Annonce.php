<?php

namespace racoin\model;

class Annonce extends \Illuminate\Database\Eloquent\Model{
	//Definit le nom de la table et de la clé primaire 
	protected $table = 'racoinannonce';
	protected $primaryKey = 'id';
	public $timestamps=false;

	function __construct()
	{}

	public function categories()
	{
		return $this->belongsTo('Categorie','idCategorie');
	}

	public static function findById($id) {

		return Annonce::where('id','=' ,$id)->get();
	}

	public static function findByCateg($categorie) {

		return Annonce::where('idCategorie','=' ,$categorie)->get();
		//return Annonce::all();
	}

	public static function add($array){

		$annonce = new Annonce;

		$annonce->titre = $array['titre'];
		$annonce->ville = $array['ville'];
		$annonce->cp = $array['cp'];
		$annonce->prix = $array['prix'];
		$annonce->nomAnnonceur = $array['nom'];
		$annonce->description = $array['description'];
		$annonce->idCategorie = $array['categ'];
		$annonce->password = $array['mdp'];
		$annonce->datePubli = $array['date'];

		$annonce->save();
	}
	
	public static function findByMail($mail) {

		return Annonce::where('mail','=' ,$mail)->get();
	}

}