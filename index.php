<?php
require 'vendor/autoload.php';
global $app;
session_start();

$app = new \Slim\Slim();
\racoin\model\Base::EloConfig();

$app->get('/',function(){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionListeAnnonce();
});

$app->get('/annonce/:id', function($id){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionDetailAnnonce($id);
});

$app->get('/annonces/:category', function($category){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
    $AnnContr->actionListAnnoncesParCateg($category);
});

$app->get('/add',  function(){  
	$AnnContr = new \racoin\controleur\AnnonceControleur();
    $AnnContr->actionAdd();
});

$app->post('/add',function() use($app){

   	$AnnContr = new \racoin\controleur\AnnonceControleur();
   
		$titre = $app->request->post('titre');
		$ville = $app->request->post('ville');
		$cp = $app->request->post('cp');
		$prix = $app->request->post('prix');
		$nom = $app->request->post('nom');

		$mdp = password_hash($app->request->post('mdp'), PASSWORD_DEFAULT, array("cost" => 12));

		$description = $app->request->post('description');
		$categ = $app->request->post('categ');
		$photo = $app->request->post('photo');
		$mail = $app->request->post('mail');
		if(isset($titre)){
			$AnnContr->actionPreviewAdd($titre,$ville,$cp,$prix,$nom,$description,$categ,$mdp,$photo,$mail);
		}



   
})->name('post');

$app->post('/add/valid',  function() use($app){

	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionValidAdd();
	$app->redirect('/racoinphp');

})->name('valid');

$app->get('/mesannonces',  function(){
	
	$AnnContr = new \racoin\controleur\AnnonceControleur();
    $AnnContr->actionListMesAnnoncesForm();
})->name('mesannoncespost');

$app->post('/mesannonces', function() use($app){

	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$mail = $app->request->post('mail');
		if(isset($mail)){
   			 $AnnContr->actionListMesAnnonces($mail);
   			 echo'coucou';
		}
});


$app->get('/aideapi', function(){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionAideApi();
});

/*
*          *
* API REST *
*          *
*/

$app->get('/json/annonce/:id', function($id){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionDetailAnnonceAPI($id);
});

$app->get('/json/annonces', function(){
	$AnnContr = new \racoin\controleur\AnnonceControleur();
	$AnnContr->actionListeAnnonceAPI();
});

$app->run();